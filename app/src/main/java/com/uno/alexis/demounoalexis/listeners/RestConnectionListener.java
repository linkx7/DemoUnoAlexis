package com.uno.alexis.demounoalexis.listeners;

/**
 * Created by Alexis on 07/02/2018.
 * Demo Interface
 */

public interface RestConnectionListener {

    void onFinished(String result);

}
