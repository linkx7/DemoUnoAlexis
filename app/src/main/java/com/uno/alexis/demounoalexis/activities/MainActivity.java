package com.uno.alexis.demounoalexis.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.uno.alexis.demounoalexis.R;
import com.uno.alexis.demounoalexis.connection.RestConnection;
import com.uno.alexis.demounoalexis.entities.Forecast;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Alexis on 07/02/2018.
 * Demo Activity that request the Location from the phone and then use the coordinates to call the
 * Forecast API and get some info to show on UI
 */
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ACCESS_LOCATION = 007;
    private static final int REQUEST_CHECK_SETTINGS = 006;
    private static final String DARKSKY_SERVER = "https://api.darksky.net/";
    private static final String API_FORECAST = "forecast/ea76e78f539ef7dae1879fd1a45d3628/";
    private static final String FORECAST_TAG = "currently";
    private static final String TIMEZONE_TAG = "timezone";

    private FusedLocationProviderClient providerClient;
    private LocationCallback locationCallback;
    private ProgressDialog dialogConnection;
    private Activity activityMain;

    // UI Elements

    private ImageButton imageButtonRefreshForecast;
    private TextView textViewTimeZone;
    private TextView textViewTemperature;
    private TextView textViewSummary;
    private TextView textViewPrecipProbabilityLabel;
    private TextView textViewPrecipProbability;
    private TextView textViewHumidityLabel;
    private TextView textViewHumidity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        // Objects initialization
        imageButtonRefreshForecast = (ImageButton) findViewById(R.id.image_button_refresh_forecast);
        imageButtonRefreshForecast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLastKnowLocation();
            }
        });
        textViewTimeZone = (TextView) findViewById(R.id.text_view_time_zone);
        textViewTemperature = (TextView) findViewById(R.id.text_view_temperature);
        textViewSummary = (TextView) findViewById(R.id.text_view_summary);
        textViewPrecipProbabilityLabel = (TextView) findViewById(R.id.text_view_precip_label);
        textViewPrecipProbability = (TextView) findViewById(R.id.text_view_precip);
        textViewHumidityLabel = (TextView) findViewById(R.id.text_view_humidity_label);
        textViewHumidity = (TextView) findViewById(R.id.text_view_humidity);

        activityMain = MainActivity.this;
        dialogConnection = new ProgressDialog(activityMain);
        dialogConnection.setMessage(getResources().getString(R.string.dialog_rest_message));
        dialogConnection.setCanceledOnTouchOutside(false);
        providerClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());

        // GetLastLocationCallback
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                for (Location location : locationResult.getLocations()) {

                    // The manager and networkInfo will be used to check the Internet connection
                    ConnectivityManager manager = (ConnectivityManager) activityMain
                            .getSystemService(CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = manager.getActiveNetworkInfo();

                    RestConnection restConnection = new RestConnection(
                            new RestConnection.RestListener() {
                                @Override
                                public void onFinished(String result) {
                                    try {
                                        dismissDialog();
                                        JSONObject jsonObject = new JSONObject(result);
                                        Gson gson = new Gson();
                                        Forecast forecast = gson.
                                                fromJson(jsonObject.getString(FORECAST_TAG), Forecast.class);
                                        forecast.setTimezone(jsonObject.getString(TIMEZONE_TAG));

                                        setForecastUI(forecast);// Take the relevant data to show them on UI

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dismissDialog();
                                    }

                                    providerClient.removeLocationUpdates(locationCallback);
                                }
                            });

                    if (networkInfo != null && networkInfo.isConnected()) {
                        // Rest API consume with the actual coordinates
                        restConnection.execute(DARKSKY_SERVER + API_FORECAST
                                + String.valueOf(location.getLatitude()) + ","
                                + String.valueOf(location.getLongitude()));
                    } else {
                        dismissDialog();
                        Toast.makeText(activityMain,
                                getResources().getString(R.string.toast_internet_disconnection),
                                Toast.LENGTH_LONG).show();
                    }

                }
            }
        };

        getLastKnowLocation();

    } // onCreate End


    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    private void dismissDialog() {
        if (dialogConnection != null) {
            dialogConnection.dismiss();
        }
    }

    /*
    * Method that get the Forecast object and set the info on the UI elements
    * */
    public void setForecastUI(Forecast forecastUI) {

        textViewTimeZone.setText(forecastUI.getTimezone());
        textViewTemperature.setText(String.valueOf(forecastUI.getTemperature()));
        textViewSummary.setText(forecastUI.getSummary());
        String numbers = "% " + String.valueOf(forecastUI.getPrecipProbability() * 100);
        textViewPrecipProbability.setText(numbers);
        numbers = "% " + String.valueOf(forecastUI.getHumidity() * 100);
        textViewHumidity.setText(numbers);
        textViewPrecipProbabilityLabel.setVisibility(View.VISIBLE);
        textViewHumidityLabel.setVisibility(View.VISIBLE);

    }


    /* This method create a LocationSettingsRequest to take the GPS position if it is enable,
        otherwise we call a system intent to enable it and try to take the GPS position */
    public void getLastKnowLocation() {
        dialogConnection.show();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(createLocationRequest());
        SettingsClient settingsClient = LocationServices
                .getSettingsClient(getApplicationContext());
        Task<LocationSettingsResponse> responseTask = settingsClient
                .checkLocationSettings(builder.build());
        responseTask.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                startLocationUpdates();
            }
        });
        responseTask.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(MainActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }


    ////////////////////////////////////////// LOCATION ////////////////////////////////////////////

    /* This method create a LocationRequest that deliver to the system the features of the
        GPS location request
         @return LocationRequest*/
    protected LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    /* This method is called when the GPS on the device has been enabled, so we can
        take the lastKnowLocation */
    private void startLocationUpdates() {

        if (ContextCompat.checkSelfPermission(activityMain,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            providerClient.requestLocationUpdates(createLocationRequest(), locationCallback, null);

        } else {

            ActivityCompat.requestPermissions(activityMain,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
        }
    }

}
