package com.uno.alexis.demounoalexis.entities;

/**
 * Created by Alexis on 07/02/2018.
 * Demo entity to take the Json
 */

public class Forecast {

    private String timezone;
    private String summary;
    private String icon;
    private double precipProbability;
    private double temperature;
    private double humidity;

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public double getPrecipProbability() {
        return precipProbability;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }
}
