package com.uno.alexis.demounoalexis.connection;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexis on 07/02/2018.
 * Demo class to connect to the server on background
 */

public class RestConnection extends AsyncTask<String, String, String> {

    private HttpURLConnection urlConnection;
    private RestListener listener;

    public interface RestListener {
        void onFinished(String result);
    }

    public RestConnection(RestListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            return MethodExecution(params[0]);
        } catch (IOException e) {
            return "Server Error";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (this.listener != null) {
            //Call the callback function.
            this.listener.onFinished(result);
        }
    }


    /* This method create a connection to the server and take the data on an InputStream object,
    but before to set the return data, we transform the InputStream into String.
         @return String with the server's response*/
    public String MethodExecution(String demoUrl)
            throws IOException {
        InputStream inputStream = null;
        try {
            URL url = new URL(demoUrl);
            urlConnection = (HttpURLConnection) url.openConnection();

            // In this case we don't have any additional features to add to the connection
            urlConnection.connect();

            inputStream = urlConnection.getInputStream();

            String contentAsString = readIt(inputStream);
            return contentAsString;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }


    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream) throws IOException {
        StringBuilder builder = new StringBuilder();
        Reader reader = new InputStreamReader(stream, "UTF-8");
        int c;
        while ((c = reader.read()) != -1) {
            builder.append((char) c);
        }
        return builder.toString();
    }


}
