package com.uno.alexis.demounoalexis;

import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;
import com.uno.alexis.demounoalexis.connection.RestConnection;
import com.uno.alexis.demounoalexis.entities.Forecast;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(AndroidJUnit4.class)
public class DemoUnoInstrumentedTest {

    static final String SERVER = "https://api.darksky.net/forecast/ea76e78f539ef7dae1879fd1a45d3628/-0.1190346,-78.4977786";
    static final String TIMEZONE = "America/Guayaquil"; //use your own TimeZone

    @Test
    public void useAppContext() throws Exception {

        RestConnection restConnection = new RestConnection(new RestConnection.RestListener() {
            @Override
            public void onFinished(String result) {
                assertNotEquals(result, null);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Gson gson = new Gson();
                    Forecast forecast = gson.
                            fromJson(jsonObject.getString("currently"), Forecast.class);
                    forecast.setTimezone(jsonObject.getString("timezone"));
                    assertEquals(forecast.getTimezone(), TIMEZONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        restConnection.execute(SERVER);

    }
}
